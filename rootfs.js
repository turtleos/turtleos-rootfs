import { defaultWallpaper } from '../../assets/turtle-wallpaper-collection/wallpapers';

import {
    File,
    Folder,
    Permission
} from '../virtfs/virtfs.js';

const PERMISSION = {
    DEFAULT: new Permission(
        {
            root:{
                canRead: true,
                canWrite: true,
                canDelete: true,
                canMake: true
            },
            user:{
            }})
};

const defaultBootConfig = new File({
    bType: 'json',
    permission: PERMISSION.DEFAULT,
    content: JSON.stringify({
        platform: 'auto',
        lang: 'en',
        bootcount: 0,
        uptime: 0,
        wallpaper: defaultWallpaper(),
        network: false,
        autonetwork: false,
        brightness: 100
    })
});

const ROOTFS = {
    buildconf: defaultBootConfig,
    etc: new Folder({
        permission: PERMISSION.DEFAULT,
        content: {
            hosts: new File({
                permission: PERMISSION.DEFAULT,
                bType: 'json',
                content: JSON.stringify({
                    routes:[
                        {
                            origin: '^(https?://)(app.)?(turtle(.os|os.ccw.icu))(?![.]).*',
                            to: 'https://duckduckgo.com' //TODO Replace with custom browser start page
                        }
                    ]
                })
            }),
            tpm: new Folder({
                permission: PERMISSION.DEFAULT,
                content: {
                    mirrorlist: new File({
                        permission: PERMISSION.DEFAULT,
                        bType:'json',
                        content: JSON.stringify({
                            official:'https://repo.turtleos.ccw.icu'
                        })
                    }),
                  /*  cache: new Folder({
                        permission: PERMISSION.DEFAULT,
                        content: {
                            mirrors: new File({
                                permission: PERMISSION.DEFAULT,
                                bType: 'json',
                                content: JSON.stringify({})
                            })
                        }
                    })*/
                }
            })
        }
    }),
    usr: new Folder({
        permission: PERMISSION.DEFAULT,
        content: {
            apps: new Folder({
                permission: PERMISSION.DEFAULT,
                content:{}
            })
        }
    }),
    home: new Folder({
        permission: PERMISSION.DEFAULT,
        content: {
        }
    })
};

export {
    PERMISSION as FilePermission,
    ROOTFS,
    defaultBootConfig
}
